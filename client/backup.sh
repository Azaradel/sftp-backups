#!/bin/bash

whatToBackup="$@"
destination="/home/user/"
day=$(date +%A)
hostname=$(hostname -s)
archiveName="$hostname-$day.tgz"

if tar czf /tmp/$archiveName $whatToBackup && sshpass -p 'haslo' scp /tmp/$archiveName user@server:$destination ; then
    echo "Subject: Backup $archiveName on $hostname has been made." > /tmp/mail$archiveName
    echo "Files in the archive: " >> /tmp/mail$archiveName
    tar tf /tmp/$archiveName | tee --append /tmp/mail$archiveName /var/log/newBackup

    echo "Differcies between new and old backup: " >> /tmp/mail$archiveName
    diff /var/log/oldBackup /var/log/newBackup >> /tmp/mail$archiveName

    echo "Archive has a sha512 checksum: " >> /tmp/mail$archiveName
    sha512sum /tmp/$archiveName >> /tmp/mail$archiveName
    ssmtp kylaver@gmail.com < /tmp/mail$archiveName

    tar tf /tmp/$archiveName > /var/log/oldBackup
    rm /var/log/newBackup
else
    echo "Subject: Backup on $hostname has failed at $day" > /tmp/mail$archiveName
    ssmtp kylaver@gmail.com < /tmp/mail$archiveName
fi